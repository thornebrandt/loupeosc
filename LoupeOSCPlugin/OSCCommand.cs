﻿namespace Loupedeck.LoupeOSCPlugin {
    using System;
    using SharpOSC;

    public class OSCCommand : PluginDynamicCommand {
        private string _channel = "/0/";
        private string _address = "10.0.0.100";
        private Int32 _port = 9000;
        private Single _float = 0.0f;
        private string _string = "";
        private string _error = "";

        public OSCCommand () : base ("OSC Command", "OSC Command", "OSC Command") {
            this.MakeProfileAction ("text; Format: IP:PORT:CHANNEL:FLOAT:STRING(optional)");
        }

        protected override void RunCommand (String _data) {
            string[] dataArray = _data.Split (':');

            //these values are stored among processes. 
            _string = "";
            _error = "";

            if (dataArray.Length > 3) {
                //data is formatted correctly, sending a float. 
                _address = dataArray[0];
                try {
                    _port = Int32.Parse (dataArray[1]);
                } catch (FormatException e) {
                    _error = e.Message;
                }
                _channel = dataArray[2];
                try {
                    _float = Single.Parse (dataArray[3]);
                } catch (FormatException e2) {
                    _error += e2.Message;
                }
            }
            if (dataArray.Length > 4) {
                _string = dataArray[4];
            }
            this.sendOSCMessage ();
        }

        private void sendOSCMessage () {
            var message = new OscMessage (_channel, _float);
            if (!string.IsNullOrEmpty (_string)) {
                message = new OscMessage (_channel, _float, _string);
            }
            if (!string.IsNullOrEmpty (_error)) {
                _string = "error";
                message = new OscMessage (_channel, _float, _string, _error);
            }
            var sender = new UDPSender (_address, _port);
            sender.Send (message);
        }
    }
}