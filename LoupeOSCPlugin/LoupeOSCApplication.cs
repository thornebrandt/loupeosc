namespace Loupedeck.LoupeOSCPlugin {
    using System;

    public class LoupeOSCApplication : ClientApplication {
        public LoupeOSCApplication () {

        }

        protected override String GetProcessName () => "LoupeOSC";

        protected override String GetBundleName () => "";
    }
}