﻿namespace Loupedeck.LoupeOSCPlugin {
    using System;
    using SharpOSC;

    public class OSCKnob : PluginDynamicAdjustment {

        private Single _increment = 0.0075f;

        private Single _value = 0.0f;
        private Single _lastValue = 0.0f;

        private string _channel = "/0/";
        private string _address = "10.0.0.100";
        private Int32 _port = 9000;
        private string _string = "";
        private string _error = "";

        //TODO - needs dictionary of addresses to knob values. 

        public OSCKnob () : base ("OSC Knob", "OSC Knob", "OSC", true) {
            this.MakeProfileAction ("text; Format: IP:PORT:CHANNEL:STRING");
        }

        protected override void RunCommand (String _data) {
            //knob push.

            this.parseData (_data);
            this.sendOSCMessage ();
        }

        private void parseData (String _data) {
            string[] dataArray = _data.Split (':');

            //these values are stored among processes. 
            _string = "";
            _error = "";

            if (dataArray.Length > 2) {
                //data is formatted correctly, sending a float. 
                _address = dataArray[0];
                try {
                    _port = Int32.Parse (dataArray[1]);
                } catch (FormatException e) {
                    _error = e.Message;
                }
                _channel = dataArray[2];
            }
            if (dataArray.Length > 3) {
                _string = dataArray[3];
            }
        }

        private void sendOSCMessage () {
            var message = new OscMessage (_channel, _value);
            if (!string.IsNullOrEmpty (_string)) {
                message = new OscMessage (_channel, _value, _string);
            }
            if (!string.IsNullOrEmpty (_error)) {
                _string = "error";
                message = new OscMessage (_channel, _value, _string, _error);
            }
            var sender = new UDPSender (_address, _port);
            sender.Send (message);
        }

        protected override String GetAdjustmentValue (String _data) {
            if (this._value != this._lastValue) {
                this.sendOSCMessage ();
                this._lastValue = this._value;
            }
            return this._value.ToString ();
        }

        protected override void ApplyAdjustment (String _data, Int32 ticks) {
            this._value += ticks * _increment;
            this._value = Clamp (this._value, 0.0f, 1.0f);
            this.parseData (_data);
            if (this._value != this._lastValue) {
                this.ActionImageChanged (_data);
            }
        }

        public static Single Clamp (Single value, Single min, Single max) {
            return (value < min) ? min : (value > max) ? max : value;
        }
    }
}