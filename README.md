# LoupeDeck OSC Plugin

### Assign Custom OSC Messages to Buttons and Knobs on LoupeDeck CT

---

## Installation 
* **Option 1:** Move the contents of the `/Builds/` folder into your LoupeDeck Plugins Library. Usually located around `C:\Users\user\AppData\Local\Loupedeck\Plugins\DemoPlugin.dll`
* **Option 2** Use Visual Studio to build the contents, which will install the plugin for you, assuming you have followed the directions on the [LoupeDeckSDK](https://github.com/Loupedeck/LoupedeckPluginSdk4/wiki/Creating-the-project-files-for-the-plugin) 
## Quick Use 

Due to the current limitations on the SDK, this plugin currently has to rely on string parsing to config the OSC messages. 

### Buttons

Button OSC events are added via a message in a profile action **OSC Command.** The address, port, and channel must be included in each custom action message.  A float must also be included.  The message must be separated by colons, and in the correct order, formatted like the following: 

  **IP:PORT:CHANNEL:FLOAT:STRING(optional)**

*Example:* `10.0.0.100:7000:/1/0.69/hello world `
*Example 2:* `53.123.23.120:8888:/chan2/1/`


### Knobs 

Knob OSC events are added via a message in a profile action **OSC Knob.** The address, port, and channel must be included in each custom action message.  An additional string is optional. The message must be separated by colons, and in the correct order, formatted like the following: 

  **IP:PORT:CHANNEL:STRING(optional)**
  
*Example:* `10.0.0.100:7000:/1/heyyyyyy `
*Example 2:* `53.123.23.120:8888:/chan2/`

Knobs are clamped and normalized between 0 and 1. 
There is a currently a known bug where all knobs share the same display value.  This only effects LoupeDeck console display. 



---

### Completed
- [x] ~~get knobs working~~
- [x] ~~cap knobs at 0 and 1~~
- [x] ~~tweak increment of knobs so that it is roughly one physical rotation from 0 to 1~~
- [x] ~~get knobs working on wheel~~


### In Progress 

- [ ] Find LoupeDesk Solutiuon for multiple string inputs per profile action setup.
- [ ] Investigate "Winforms" as hack. 

### TODO 

- [ ]  Make separate actions for string events vs float events. 
- [ ] Make separate knob action for continuous infinite values. 
- [ ] Make separate action for OSC toggle.  
- [ ] Fix knobs sharing display values. Make Dictionary for each discovered event string so that knobs do not conflict with each other. ( currently all knob values are being shared, and overwritten ) 
- [ ] Listen for OSC changes that match knob and update values to display synced. 
- [ ] Dynamic min/max values
- [ ] Dynamic increments 


